<!doctype html>
<html lang="en">
	<head>
		
		<meta charset="utf-8">
		<meta http-equiv="x-ua-compatible" content="ie=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		
		<title>Cebu Ocean Park – Oceanarium and Convention Arena in the Philippines</title>
		<link rel="icon" type="image/png" href="images/cop.ico">
		<meta name="author" content="Cebu Ocean Park">
		<meta name="description" content="Cebu Ocean Park is the largest world-class marine theme park in the Philippines with function spaces and a food hall for family getaways and events."/>
		<meta name="keywords" content="cebu ocean park, theme park, convention arena, events, ocean park, marine theme park, lifestyle destination, function spaces, meetings, convention, marine attractions, Oceanarium, aquarium, family getaway, food hall, Cebu, travel, lifestyle destination, cebu ocean park price, ocean park schedule, mermaid swim experience, jungle trek, fish spa, crocodile, underwater, aquanaut, glass bottom ride, stingray interaction, mermaid swim encounter, bird feeding, animals, cebu attractions, SM seaside city cebu"/>
		
		<meta property="og:url" content="http://www.cebuoceanpark.com" />
		<meta property="og:title" content="Cebu Ocean Park – Oceanarium and Convention Arena in the Philippines" />
		<meta property="og:description" content="Cebu Ocean Park is the largest world-class marine theme park in the Philippines with function spaces and a food hall for family getaways and events."/>
		<meta property="og:type" content="website" />
		<meta property="og:image"  content="http://www.cebuoceanpark.com/images/Cebu%20Ocean%20Park%20.jpg"/>
		<link rel="stylesheet" href="css/normalize.css">
		<link href="https://fonts.googleapis.com/css?family=Nunito+Sans:400,700,900" rel="stylesheet">
		<link rel="stylesheet" href="css/style.css?v=1.0.<?php echo time();?>">
		<link rel="stylesheet" href="css/style.mobile.css?v=1.0.<?php echo time();?>">
		<link rel="stylesheet" href="css/jquery.slide.css?v=1.0.<?php echo time();?>">
		
		<!--[if lt IE 9]>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.js"></script>
		<![endif]-->
	</head>
<body>
	<header>
		<div class="container">
			<h1>
				<img class="logo" src="images/logo.png" alt="Cebu Ocean Park" />
			</h1>
			<div class="unveil">Unveiling soon..</div>
			<nav>
				<ul>
					<li>
						
							<img src="images/cop-string-ray.png" alt="Aquatic attractions" />
							<span> AQUATIC <br/> ATTRACTIONS</span>
						
					</li>
					<li>
						
							<img src="images/cop-spoon-fork.png" alt="Delectable and exciting dinning concepts" />
							<span>DELECTABLE <span class="sm-hidden">AND EXCITING</span> <br/> DINING <span class="sm-hidden">CONCEPTS</span></span>
						
					</li>
					<li>
						
							<img src="images/cop-table.png" alt="Enchanting venue spaces" />
							<span>ENCHANTING <br/> <span class="xs-hidden">VENUE</span>  SPACES</span>
						
					</li>
				</ul>
			</nav>
			
			
		</div>
	</header>
	<section id="main">
	<div class="slide">
      <ul>
        <li data-bg="images/sliders/slide-1.jpg"></li>
        <li data-bg="images/sliders/slide-2.jpg"></li>
      </ul>
    </div>
		<div class="content">
			<h2>THE LARGEST OCEANARIUM IN THE PHILIPPINES<br/>
				WITH FUNCTION SPACES AND A FOOD HALL<br/>
				IS COMING TO CEBU!
			</h2>
			<p>Indulge in a unique underwater experience. </p>
			<div id="sneak-peak" class="action-block">
				<span>Take a sneak peek</span>
				<a href="Cebu Ocean Park Factsheet.pdf" target="_blank" alt="COP Factsheet">Download Factsheet</a>
			</div>
			<div id="book-event" class="action-block">
				<span>Book your event with us</span>
				<a id="meeting" href="Cebu Ocean Park Factsheet.pdf" target="_blank" alt="Meetings & Events Overview"><div>Meetings & Events</div> Overview</a>
				<a id="enquire" href="Cebu Ocean Park Factsheet.pdf" target="_blank" alt="Enquire with us"><div>Enquire with us</div></a>
			</div>
		</div>
	</section>
	<section id="contact">
		<div class="container">
			<div class="card">
				<div>
					<h3 class="title">
						EXCITING CAREERS AWAIT
					</h3>
					<p class="details">
						<img src="images/cop-map-marker.png" alt="" />
						Cebu Ocean Park,<br/>
						Cebu City, Philippines 1000
					</p>
					<a href="https://goo.gl/forms/BKzdRQQJ1MWy6oJX2" target="_blank" class="action-button">APPLY HERE</a>
				</div>
			</div>
			<div class="card">
					<div>
					<h3 class="title">
						CONTACT US
					</h3>
					<p class="details">
							<img src="images/cop-phone.png" alt="" />
							(+632) 7557 888 <br/>
							<img src="images/cop-email.png" alt="" />
							info@cebuoceanpark.com
					</p>
					<a href="https://goo.gl/forms/LIjfR30yrPUMnYfn2"  target="_blank" class="action-button">SUBMIT AN ENQUIRY</a>
					</div>
			</div>
			<div class="card">
				<div>
					<h3 class="title">
						BE THE FIRST TO KNOW<br/>
						WHEN WE OPEN
					</h3>
					<p>&nbsp;</p>
					<a href="http://eepurl.com/dBc-J9" target="_blank" class="action-button">
						SIGN UP FOR MAILING LIST
					</a>
				</div>
			</div>
		</div>
	</section>
	<div class="share-icons">
		<ul>
			<li><a href="https://www.facebook.com/CebuOceanPark/" target="_blank"><img src="images/cop-fb.png" alt="Cebu Ocean Park on Facebook" /></a></li>
			<?php if(0): ?>
			<li><a href="https://twitter.com/CebuOceanPark"  target="_blank"><img src="images/cop-twitter.png" alt="Cebu Ocean Park on  Twitter" /></a></li>
			<?php endif;?>
			<li><a href="https://www.instagram.com/cebuoceanpark/"  target="_blank"><img src="images/cop-instagram.png" alt="Cebu Ocean Park on Instagram" /></a></li>
			<?php if(0): ?>
			<li><a href=""><img src="images/cop-youtube.png" alt="Youtube" /></a></li>
			<?php endif;?>
		</ul>
	</div>
	<?php if($showFooter=false): ?>
	<footer>
		<div class="logo">
			<img src="images/logo.png" alt="Cebu Ocean Park" />
		</div>
		<ul>                   
			<li><a href="">CEBU OCEAN PARK</a></li>
			<li><a href="">CONTACT US</a></li>
			<li><a href="">TERMS OF USE</a></li>
			<li><a href="">PRIVACY POLICY</a></li>
		</ul>
	</footer>
	<?php endif; ?>
	<script src="https://code.jquery.com/jquery-1.12.4.min.js" integrity="sha384-nvAa0+6Qg9clwYCGGPpDQLVpLNn0fRaROjHqs13t4Ggj3Ez50XnGQqc/r8MhnRDZ" crossorigin="anonymous"></script>
    <script src="js/jquery.slide.min.js"></script>
    <script type="text/javascript">
      $(function() {
        $('.slide').slide({
          'slideSpeed': 3000,
          'isShowArrow': true,
          'dotsEvent': 'mouseenter',
          'isLoadAllImgs': true
        });
      });
    </script>
</body>
</html>